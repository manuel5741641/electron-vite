import { useState } from 'react';
import './App.css';
import { myFunction } from './script';

function App() {
 
  return (
    <>
     <div className="video-container" id="videoContainer">
  <video id="videoElement" width="1080" autoPlay loop muted>
    <source src="convert.mp4" type="video/mp4" />
          Tu navegador no soporta el elemento de video.
        </video>
      </div>
    </>
  );
}

export default App;
