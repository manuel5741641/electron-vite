export function myFunction() {
    document.addEventListener("DOMContentLoaded", function() {
        var videoContainer = document.getElementById('videoContainer');
        var video = document.getElementById('videoElement');
      
        // Asegúrate de que el video no sea nulo y esté cargado
        video.addEventListener('loadedmetadata', function() {
          var videoDuration = video.duration;
          var scrollHeight = videoContainer.scrollHeight - videoContainer.clientHeight;
      
          videoContainer.onscroll = function() {
            var scrollPosition = videoContainer.scrollTop;
            var time = (scrollPosition / scrollHeight) * videoDuration;
            video.currentTime = time;
          };
        });
      });
      
  }
  